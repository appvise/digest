const M    = require('../');
const test = require('tape');

test('Method exports', t => {
  t.plan(26);

  // Main functions (7)
  t.equal(typeof M             , 'object'  , 'Module is an object');
  t.equal(typeof M.hex_md5     , 'function', '\'hex_md5\' is a function');
  t.equal(typeof M.b64_md5     , 'function', '\'b64_md5\' is a function');
  t.equal(typeof M.any_md5     , 'function', '\'any_md5\' is a function');
  t.equal(typeof M.hex_hmac_md5, 'function', '\'hex_hmac_md5\' is a function');
  t.equal(typeof M.b64_hmac_md5, 'function', '\'b64_hmac_md5\' is a function');
  t.equal(typeof M.any_hmac_md5, 'function', '\'any_hmac_md5\' is a function');

  t.equal(typeof M.md5_vm_test     , 'function', '\'md5_vm_test\' is a function');
  t.equal(typeof M.rstr_md5        , 'function', '\'rstr_md5\' is a function');
  t.equal(typeof M.rstr_hmac_md5   , 'function', '\'rstr_hmac_md5\' is a function');
  t.equal(typeof M.rstr2hex        , 'function', '\'rstr2hex\' is a function');
  t.equal(typeof M.rstr2b64        , 'function', '\'rstr2b64\' is a function');
  t.equal(typeof M.rstr2any        , 'function', '\'rstr2any\' is a function');
  t.equal(typeof M.str2rstr_utf8   , 'function', '\'str2str_utf8\' is a function');
  t.equal(typeof M.str2rstr_utf16le, 'function', '\'str2str_utf16le\' is a function');
  t.equal(typeof M.str2rstr_utf16be, 'function', '\'str2str_utf16be\' is a function');
  t.equal(typeof M.rstr2binl       , 'function', '\'rstr2binl\' is a function');
  t.equal(typeof M.binl2rstr       , 'function', '\'binl2rstr\' is a function');
  t.equal(typeof M.binl_md5        , 'function', '\'binl_md5\' is a function');
  t.equal(typeof M.md5_cmn         , 'function', '\'md5_cmn\' is a function');
  t.equal(typeof M.md5_ff          , 'function', '\'md5_ff\' is a function');
  t.equal(typeof M.md5_gg          , 'function', '\'md5_gg\' is a function');
  t.equal(typeof M.md5_hh          , 'function', '\'md5_hh\' is a function');
  t.equal(typeof M.md5_ii          , 'function', '\'md5_ii\' is a function');
  t.equal(typeof M.safe_add        , 'function', '\'safe_add\' is a function');
  t.equal(typeof M.bit_rol         , 'function', '\'bit_rol\' is a function');

});
