ALGS:=
ALGS+=sha-1

.PHONY: default
default: node_modules dist

dist: node_modules $(patsubst %, %/dist, $(ALGS))
	npm run build:code

node_modules: $(patsubst %, %/node_modules, $(ALGS))
	npm install

.PHONY: release
release: dist
	npm publush

%/dist: %/node_modules
	cd $(shell dirname $@) && npm run build:code

%/node_modules:
	cd $(shell dirname $@) && npm install

.PHONY: clean
clean:
	cd sha-1 && rm -rf dist
	cd sha-1 && rm -rf node_modules
	rm -rf dist
	rm -rf node_modules
