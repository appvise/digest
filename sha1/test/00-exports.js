const M    = require('../');
const test = require('tape');

test('Method exports', t => {
  t.plan(23);

  // Main functions (7)
  t.equal(typeof M              , 'object'  , 'Module is an object');
  t.equal(typeof M.hex_sha1     , 'function', '\'hex_sha1\' is a function');
  t.equal(typeof M.b64_sha1     , 'function', '\'b64_sha1\' is a function');
  t.equal(typeof M.any_sha1     , 'function', '\'any_sha1\' is a function');
  t.equal(typeof M.hex_hmac_sha1, 'function', '\'hex_hmac_sha1\' is a function');
  t.equal(typeof M.b64_hmac_sha1, 'function', '\'b64_hmac_sha1\' is a function');
  t.equal(typeof M.any_hmac_sha1, 'function', '\'any_hmac_sha1\' is a function');

  // Internal (but exported) functions (16)
  t.equal(typeof M.binb2rstr       , 'function', '\'binb2rstr\' is a function');
  t.equal(typeof M.binb_sha1       , 'function', '\'binb_sha1\' is a function');
  t.equal(typeof M.bit_rol         , 'function', '\'bit_rol\' is a function');
  t.equal(typeof M.rstr2any        , 'function', '\'rstr2any\' is a function');
  t.equal(typeof M.rstr2b64        , 'function', '\'rstr2b64\' is a function');
  t.equal(typeof M.rstr2binb       , 'function', '\'rstr2binb\' is a function');
  t.equal(typeof M.rstr2hex        , 'function', '\'rstr2hex\' is a function');
  t.equal(typeof M.rstr_hmac_sha1  , 'function', '\'rstr_hmac_sha1\' is a function');
  t.equal(typeof M.rstr_sha1       , 'function', '\'rstr_sha1\' is a function');
  t.equal(typeof M.safe_add        , 'function', '\'safe_add\' is a function');
  t.equal(typeof M.sha1_ft         , 'function', '\'sha1_ft\' is a function');
  t.equal(typeof M.sha1_kt         , 'function', '\'sha1_kt\' is a function');
  t.equal(typeof M.sha1_vm_test    , 'function', '\'sha1_vm_test\' is a function');
  t.equal(typeof M.str2rstr_utf16be, 'function', '\'str2rstr_utf16be\' is a function');
  t.equal(typeof M.str2rstr_utf16le, 'function', '\'str2rstr_utf16le\' is a function');
  t.equal(typeof M.str2rstr_utf8   , 'function', '\'str2rstr_utf8\' is a function');
});
